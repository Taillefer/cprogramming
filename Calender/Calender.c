/*****************************************************************************************************************************************************************
	Begin #include Statement 
*****************************************************************************************************************************************************************/
#include <stdio.h>
#include <assert.h>
#include "Calender.h"
#include <wchar.h>
#include "Table.h"

static int day = 0; 
static int month;
static int year;
static int daysFrom;
static int MonthFactor[] = {1,4,4,0,2,5,0,3,6,1,4,6}; //Factor of every month provided by David Houtman
static int daysPerMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31}; // The days in each month
static unsigned int offset;
static int currentDay = 0;

/*****************************************************************************************************************************************************************
	
	Purpose: Calculates what the offset in the calender should be given the current year and month

	History: Part of code taken from David Houtman November 1, 2014

	INPUTS: The calender year, and calender month
	
	OUTPUTS: The amount of Offset the calender should use

	ALGORITHM(S) : Calculates the offset by using the algorithm provided by david houtman. 
*****************************************************************************************************************************************************************/

int calculateDayOfWeek(int years, int month) {
	year = years % 2000;  //Take the last 2 digits of the year
	offset = (year + (int)(year/4) + ((isLeapYear(years) && (month==1 || month==2) ? (MonthFactor[month-1] -1) : (MonthFactor[month-1])) )+6) % 7;
	return offset;
}

/*****************************************************************************************************************************************************************
	
	Purpose: Calculates if the current year is a leap year

	History: Part of code taken from previous lectures, November 1, 2014

	INPUTS: The calender year
	
	OUTPUTS: 1 if the year is a leap, 0 if not

	ALGORITHM(S) : Checks to see if the year is divisible by 400 or not divisible by 100 and divisible by 4 
*****************************************************************************************************************************************************************/

int isLeapYear(int years) {
	if ((years%400==0 || years%100 != 0) && (year%4==0)) { // if the year is divisble by 400 or not by 100 and divisble by 4 than its a leap year
		return 1;
	}
	else { // else return false;
		return 0; 
	}
}

int getDaysForMonth(int months) {
	return (unsigned int)daysPerMonth[months-1];
}

/*****************************************************************************************************************************************************************
	
	Purpose: Set the instance variables for Month and Year, with assert's to ensure the year is greater than 0 and the month between 1 and 12

	History: Created by Brodie Taillefer, Nov 1 , 2014

	INPUTS: The calender year, and calender month
	
	OUTPUTS: The amount of Offset the calender should use

	ALGORITHM(S) : Calculates the offset by using the algorithm provided by david houtman. 
*****************************************************************************************************************************************************************/

void getCalenderDate() {
	wprintf(L"Month: \n");
	scanf("%d",&month); // Read in the user input for month
	assert(month > 0 && month <=12); // assert month is in range
	wprintf(L"Year: \n");
	scanf("%d",&year); // Read in the user input for year
	assert(year > 1999); // assert year is in range
}

int getYear() {
	return year;
}

int getMonth() {
	return month;
}				

/*****************************************************************************************************************************************************************
	
	Purpose: Find out what the current day of the year is. Finds the cummulative day and takes into account leap year.

	History: Created by Brodie Taillefer, Nov 1 , 2014

	INPUTS: The current day of the month.
	
	OUTPUTS: The current cummulative day.

	ALGORITHM(S) : If the month is greater than 0, than iterate through the previous months adding together their cummulative days. Then begin to add
					the current date in the month to get the total. If the month is january, the total days will be the current  
*****************************************************************************************************************************************************************/

int getCurrentDay(int counter) {
	int dayIndex; // for loop counter
	currentDay = 0; //set the current day as 0 
	if(getMonth() > 1) { // if the month is greater than january
		for(dayIndex=0;dayIndex<getMonth();dayIndex++) { //loop through the month -1 
			currentDay += getDaysForMonth(dayIndex); // add how many days were in that month to the currentDay
		}
		currentDay += counter;	// add the current month and its day
	}
	else if(getMonth() == 1) { // if month is january
		currentDay += counter; // add current days to day
	}
	return currentDay;	//return total number of days
}

/*****************************************************************************************************************************************************************
	
	Purpose: Call functions to succesfully print out the calender

	History: Created by Brodie Taillefer, Nov 1 , 2014

	INPUTS: void function
	
	OUTPUTS: no return, calls the functions to draw the calender

	ALGORITHM(S) : N/A 
*****************************************************************************************************************************************************************/
void drawCalender() {
	checkTable(); // Make sure terminal window is large enough
	getCalenderDate(); //Get user input for month and year
	setTableOffSet(calculateDayOfWeek(getYear(),getMonth())); //set the table offset
	outputTable(); // draw the calender
}
