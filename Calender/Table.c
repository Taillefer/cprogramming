/*****************************************************************************************************************************************************************
	Begin #include Statement 
*****************************************************************************************************************************************************************/

#include <stdio.h>
#include <locale.h>
#include <wchar.h>
#include <assert.h>
#include "BoxDrawChars.h"
#include "Calender.h"
#include "ConsoleData.h"
#include "Table.h"
#include "HolidayInfo.h"

/*****************************************************************************************************************************************************************
	Begin #define Statement 
*****************************************************************************************************************************************************************/

#define assert__(x) for ( ; !(x) ; assert(x) )  /*  Assert macro to allow an error message */

/* Output the the first/last/middle lines of the calender */
#define outputFirst(type,col,wid) \
			wprintf(type[LEFT]); \
			for(i=0;i<col-1;i++) { \
				for(j=0;j<wid;j++) { \
					wprintf(type[BAR]); } \
						if(i!=col-2) { \
							wprintf(type[MIDDLE]);}} \
 						wprintf(type[RIGHT]);

/* Output the Height and spaces of the calender aswell as any numbers and holidays */
#define outputHeight(col,wid) \
			for(i=0;i<col;i++) { \
				wprintf(intern[LEFT]); \
				if(flag==0){ \
					getBoxString();} \
						else { \
							if(((modCounter+2) % 4) == 0) { \
								getBoxSpaces(); dayCounter++;} \
							else { wprintf(L"                ");}}} \
							wprintf(intern[RIGHT]); wprintf(CR);flag=1;modCounter++;

static int nColumns = 7;
static int mRows = 5;
static int wWidth = 16;
static int hHeight = 4;
static int i,j,p,rows=0; 	//Variables to keep track of loops	
static int counter = 1;		//Keep track of the what day to print 
static int flag = 0;		//Keep track of if the date should should be printed
static int offSet = 0;		// Offset of the start of the calender
static int numOfBoxes = 0;
static int dayCounter = 1; //keep track of the first line of day's
static int flags = 0;	   //Flag to keep track on what line to print out the holiday
static int modCounter = 1; //keep track of the 2nd line of day's

/*****************************************************************************************************************************************************************
	
	Purpose: Output the table for the calender to the screen

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: void method
	
	OUTPUTS: no return value, calls the macro functions to write the table to the screen line by line

	ALGORITHM(S) : See pseudo code for full logic
*****************************************************************************************************************************************************************/

void outputTable() {
	int daysOfMonth = getDaysForMonth((int)(getMonth())); 	// get how many months are in the current Month
	flags=0;
	if(offSet !=1) {					// if there is an offset, print out the first line of offset
		outputFirst(offset,offSet,wWidth);
	}
	outputFirst(first,(nColumns+2)-offSet,wWidth);		// Print out the first line of first's
	wprintf(CR);										  
	flags = 0;
	for(rows=0;rows<hHeight;rows++) {			// for loop to begin the output of the offset and height of the first box
		if(offSet !=1) {
			outputFirst(offset,offSet,wWidth);
		}
		outputHeight((nColumns+1) - offSet,wWidth);		
	}
	flag = 0;flags = 0;					// first box done, reset the flags to allow the date to be printed again
	if(offSet !=1) {									
		outputFirst(first,offSet,wWidth);		// If there was an offset, print out first again for x offset
	}
	outputFirst(middle,(nColumns+2) - offSet, wWidth);	// Print out the middle for x boxes that are left in the line
	wprintf(CR);
	for(p=0;p<mRows-2;p++) {				// for loop to print out x rows that are left
		for(rows=0;rows<hHeight;rows++) {		// for loop to print out x height for each row
			outputHeight(nColumns,wWidth);
		}
		if(p != 2 || offSet == 7) {			// if the box isnt on the last row, or has a max offset, print out the middle
			outputFirst(middle,nColumns+1,wWidth);
			wprintf(CR);
			flag=0;flags=0;				// Reset flags to show date
		}
		else {
			if(offSet != 6 || daysOfMonth == 30 || daysOfMonth == 28 || daysOfMonth == 29) { // Check to see if the calender doesnt start on a friday
				outputFirst(middle,getDaysLeft(),wWidth);
			}
			else {
				if(daysOfMonth == 31) {		// If the calender loops around onto the next line we must subtract the boxes by 1
					outputFirst(middle,getDaysLeft()-1,wWidth);
				}
			}
			wprintf(L"\b");
			if(getDaysLeft() <=nColumns) {		// If there is not a full 7 days to print out, how many days are left.
				outputFirst(last,(nColumns-getDaysLeft())+2,wWidth);
			}
			flags=0;flag=0;				// Reset flags for date
			wprintf(CR);
		}
	}
	for(rows=0;rows<hHeight;rows++) {			// Print out the height for the last row, decide how many boxes are left to print
		if(getDaysLeft() <=nColumns) {			// If more less than 7 days to print, print out days left
			outputHeight(getDaysLeft()-1,wWidth);	
		}
		else {
			outputHeight(nColumns,wWidth);		//else print out 7 days
		}
	}
	if(getDaysLeft() <=(nColumns+1)) {	// If the days was less than 7, print out last to close the boxes, if more we must apply middle for x boxes 
		outputFirst(last,getDaysLeft(),wWidth);
	}
	else {
		flags=0;flag = 0;				// Reset flags for date
		outputFirst(last,nColumns+1,wWidth);		// output last to close the boxes.
		wprintf(CR);
		for(rows=0;rows<hHeight;rows++) {    // on the odd time the calender starts on a friday and has 31 days we must do a new line and add 1 more box
			outputHeight(getDaysLeft()-(nColumns+1),wWidth);
		}
		outputFirst(last,getDaysLeft()-nColumns,wWidth);
	}
}


/*****************************************************************************************************************************************************************
	
	Purpose: Output the spaces in the table, pads the string with upto 16 spaces

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: void method
	
	OUTPUTS: no return value, outputs the spaces into the calender depending on the date

	ALGORITHM(S) : Pads string with 16 spaces
*****************************************************************************************************************************************************************/

void getBoxString() {
	wprintf(L"%16d",counter); //pad string with upto 16 spaces depending on counter size
	counter++;
}

/*****************************************************************************************************************************************************************
	
	Purpose: Output's the holiday into the calender if the current date is a holidy

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: void method
	
	OUTPUTS: no return value, outputs the string of the holiday padded with spaces 

	ALGORITHM(S) : if the current day is a holiday, than create a new char array to hold the holiday string, concat the string with day and left pad to 16 chars,
					if not a holiday print 16 spaces.
*****************************************************************************************************************************************************************/
void getBoxSpaces() {
	if(getHolidayStringLength(getCurrentDay(dayCounter)) > 0 ) { //check to see if the current dates holiday string length is greater than 0
		char str[18];	//Create a char array to hold the holiday
		snprintf(str, sizeof str, "%s%s", getHolidayString(getCurrentDay(dayCounter)), " Day"); // concat the holiday str with the word " day"
		wprintf(L"%16s",str);	//pad the string to the width of the box
	}
	else {
		wprintf(L"                "); //if not a holiday print out the width of the box
	}
}

/*****************************************************************************************************************************************************************
	
	Purpose: Set the instance variable for the table offset and number of visible boxes left

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: offset and number of visible boxes
	
	OUTPUTS: no output, intializes the instance variables offSet and numOfBoxes

	ALGORITHM(S) : Set the tableoffset and numOfBoxes through the parameters
*****************************************************************************************************************************************************************/
void setTableOffSet(unsigned int offset, unsigned int numOfVisibleBoxes) {
	offSet = offset + 1;  // Set the offSet variable as the offset  + 1
	numOfBoxes = numOfVisibleBoxes; // Set the numOfBoxes variable 
}


/*****************************************************************************************************************************************************************
	
	Purpose: Get how many days are currently left to print out regarding the last row

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: void method
	
	OUTPUTS: no return value

	ALGORITHM(S) : Checks to see if the year being outprinted is a leapyear, and if the month is february. Get how many days are in the month, and subtract that 					   by how many days were in the first week, and add the next 3 weeks. This will return how many days are left to print out
*****************************************************************************************************************************************************************/

int getDaysLeft() {
	if(isLeapYear(getYear()) != 1 || getMonth() !=2 ) {		// Check to see if the current year is a leap and if its not february
		return getDaysForMonth(getMonth()) - ((nColumns - offSet) + (3 * 7)); // return the amount of days left in the year, after the 3rd week
	}
	else if( isLeapYear(getYear()) == 1 && getMonth() == 2) { // If it is a leap year and february, than add 1 to how many days are left to print out
		return (getDaysForMonth(getMonth()) +1) - ((nColumns - offSet) + (3 * 7));
	}	
}

/*****************************************************************************************************************************************************************
	
	Purpose:	Checks to see if the console is greater than the lowest width allowed

	History: Created by Brodie Taillefer, November 1, 2014

	INPUTS: void method
	
	OUTPUTS: no return value

	ALGORITHM(S) : Uses the method provided in ConsoleData.c to grab the terminal's window's char width. Uses the assert macro we created to make sure the program
					will only run if the char width is greater than 115, asserts an error message if it isn't.
*****************************************************************************************************************************************************************/

void checkTable() {
	assert__(getConsoleWidthChars() > 115) { // Use assert macro to check to see if the terminal console width is greater than the minimum allowed 
		wprintf(L"Please resize the terminal window"); //Error message to be printed
	}
}



