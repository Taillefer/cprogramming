/*****************************************************************************************************************************************************************
	Begin #include Statement 
*****************************************************************************************************************************************************************/

#include <wchar.h>
#include <locale.h>
#include <assert.h>
#include <string.h>

typedef const unsigned int CINT;
typedef char* string;


/*****************************************************************************************************************************************************************
	Begin #define Statement 
*****************************************************************************************************************************************************************/

#define NYD 1
#define GHD 33
#define VD 45
#define SPD 76
#define AFD 91
#define RD 315
#define CD 359
#define BD 360

CINT NEW_YEARS_DAY = NYD;
CINT GROUND_HOG_DAY = GHD;
CINT VALENTINES_DAY = VD;
CINT ST_PATRICKS_DAY = SPD;
CINT APRIL_FOOLS_DAY = AFD;
CINT REMEMBRANCE_DAY = RD;
CINT CHRISTMAS_DAY = CD;
CINT BOXING_DAY = BD;

CINT HolidayDayOfYear[] = {NYD, GHD, VD, SPD, AFD, RD, CD, BD};

string Holidays[] = {"New Year's", "Ground Hog", "Valentine's", "St. Patricks", "April Fool's", "Remembrance", "Christmas", "Boxing"};

/*****************************************************************************************************************************************************************
	
	Purpose: Returns the index of the holiday in the HolidayDayYear Array, if the currentDay is a holiday.

	History: Code given by David Houtman

	INPUTS: Current day in the year
	
	OUTPUTS: The index of the holiday if it is a holidy

	ALGORITHM(S) : iterates through the HolidayDayOfYear and returns an index if the holiday exists 
*****************************************************************************************************************************************************************/

static int getHolidayIndex(CINT dyOfYr){
   int ctr = -1;
   assert((dyOfYr < 366) || (dyOfYr > 0));
   while ((dyOfYr != HolidayDayOfYear[++ctr]) && ctr < 8){}
   return((ctr > 7) ? -1: ctr);
}

/*****************************************************************************************************************************************************************
	
	Purpose: Returns the length of the holiday string if it a holiday, if not return 0

	History: Part of code taken from David Houtman November 1, 2014

	INPUTS: Current day of the year
	
	OUTPUTS: The length of the str, if not a holiday returns 0

	ALGORITHM(S) : Takes the length of the holiday if found by using the algorithm provided by david houtman. 
*****************************************************************************************************************************************************************/

size_t getHolidayStringLength(CINT dyOfYr){
   int holidayIndex = getHolidayIndex(dyOfYr);
   return(((holidayIndex < 0)  || (holidayIndex > 8))?0:strlen(Holidays[holidayIndex]));
} 

/*****************************************************************************************************************************************************************
	
	Purpose: Returns the string of the Holiday if the index is found

	History: Part of code taken from David Houtman November 1, 2014

	INPUTS: Current day of the year
	
	OUTPUTS: The actual string of the holiday, if not found returns NULL

	ALGORITHM(S) : Returns the string of the holiday if found by using the algorithm provided by david houtman. 
*****************************************************************************************************************************************************************/
string getHolidayString(CINT dyOfYr){
   int holidayIndex = getHolidayIndex(dyOfYr);
   return((holidayIndex < 0)? NULL:Holidays[holidayIndex]);
}



