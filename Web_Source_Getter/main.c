#include "getHTMLPage.h"
#include <ctype.h>
#include "parseURL.h"

/* Create a structure to hold the set of domain and fragments */
struct website {
	char* domain;
	char* fragment;
}URLS[300] = {'\0'};

int main(void) {
	char HTMLSource[BUFSZ] = {'\0'}; //Create the char array to hold the HTMLSource, size of 100KB
	char fullURL[BufSize], *pFullURL = fullURL; //Char array to hold the full URL
	char domain[BufSize], *pDomain = domain; //Char array to hold the domain(base) URL
	char fragment[BufSize], *pFragment = fragment; //Char array to gold the fragment URL
	printf("Enter the full url you wish to analyze: "); //Use must enter an address with a fragment or we will get a seg fault
	scanf("%s",pFullURL);
	URLSplit(pFullURL,pDomain, pFragment); //Take the user's entered URL and split it into base and fragment, 
	char tempDomain[BufSize]; strcpy(tempDomain,pDomain); //Copy the domain into the tempDomain, majority of addresses found will have this domain
	printf("Domain: %s ---- Fragment: %s ---\n",domain,fragment);
	URLS[0].domain = pDomain; //The first array of structures will be intialized to the user's webpage choice, set the main domain
	URLS[0].fragment = pFragment; //Set the first fragment
	while(1) { 
  		getHTML(URLS[0].domain,URLS[0].fragment, HTMLSource); //Initialize the HTMLSource with the the default address, make sure it is a valid address before proceeding
		int i;
		for(i=0;HTMLSource[i];++i) { //For loop to lowercase the entire html source, so we don't run into false positives.
			HTMLSource[i] = tolower(HTMLSource[i]); //toLower the current index
		}

		char *address;
		while((address = strstr(HTMLSource,"<base href=")) != NULL) { // While there is a <base href that exists, this should only happen once
			address+=12; //Increment address to get past the <base href="
			char tempAddress[100000]; strcpy(tempAddress,address); //Take a copy of the address, the address will be destroyed by strtok
			pDomain = strtok(address,"\">"); //String tokenize to get the domain, everthing from start to />, closing base tag
			address = tempAddress; //Reset the address 
			break;
		}
		if(address == NULL) { //If no base tag was found we must intialize the address or we will seg fault
			address = HTMLSource;
		}
		int counter = 0;
		while((address = strstr(address,"<a href=\"")) != NULL) { //Check to find the next <a href tag, if it exists
			char *TempAddress = malloc(BUFSZ); strcpy(TempAddress,address); //Take a copy of the address because it will be destroyed by strtok	
			pFragment = findURL(address); //Pointer to fragment will get the address's href URL
			address = TempAddress;
			address+=1; //Increase address to find new <a href tag

			if(strstr(fragment,"http") != NULL && strstr(fragment,pDomain) == NULL) { //If the fragment contains http and not the default domain, it is an external site
				URLSplit(pFragment,pDomain,pFragment); //Split the fragment into domain and fragment
				URLS[counter+1].domain = pDomain;
				URLS[counter+1].fragment = pFragment;
				pDomain = tempDomain; //Reset the domain back to the original
			}	
			else if(strstr(fragment,pDomain) == NULL) { //The link must be a relative link than it, /Youth, we must just cat the base and frag
				pDomain = tempDomain;
				URLS[counter+1].domain = pDomain;
				URLS[counter+1].fragment = pFragment;
			}
			counter++;
		}
	char userChoice;
	int codeChoice;
	int c;
	
	while(userChoice != '0') { //while user wants to view another web page
		printf("Please make a selection: 0 to quit : Anything else to view code:  ");
		scanf("%c",userChoice);
		fflush(stdin); //Clear buffer for scanf char bug
		printf("\n");
		for(c=0; c < counter; c++) { //Print out all the < a href tags found, with a number before
			printf("%d -- %s%s\n",c,URLS[c].domain,URLS[c].fragment);
		}
		printf("What number would you like to view? : "); //What webpage source does the user want to choose?
		scanf("%d",&codeChoice);
		fflush(stdin); //Flush the buffer
		printf("%s%s",URLS[codeChoice].domain,URLS[codeChoice].fragment); //Print out the user's choice
		getHTML(URLS[codeChoice].domain, URLS[codeChoice].fragment, HTMLSource); //Set the HTMLSource to the user's webpage they chose
		printf("%s\n\n\n\n\n",HTMLSource); //Print out the website source
	}
}
		
}
	

