#include "parseURL.h"

/**********************************************************************
PURPOSE: Take a completeURL and split it into it's domain and fragments
HISTORY: Created by Brodie Taillefer, December 7,2014
INPUTS: Pointer to completeURL, pointer where to store the domain and fragment
OUTPUTS: The values into the domain and fragment
ALGORITHM(S): Takes the domain and splits it on the "/", and everything after +
	the "/" are the fragment
**********************************************************************/

void URLSplit(char *completeUrl, char *domain, char *fragment) {
	sscanf(completeUrl, "%50[^/\n]", domain);
	strcpy(fragment, strchr(completeUrl, '/'));
}

/**********************************************************************
PURPOSE: Concat a base and fragment into a full URL
HISTORY: Created by Brodie Taillefer, December 7,2014
INPUTS: Pointer to the base and fragment values
OUTPUTS: The website url
ALGORITHM(S): Creates a local char array and pointer that strcat's together the base and
	fragment into 1 useable URL
**********************************************************************/

char *URLCat(char *base, char *fragment) {
	char completeURL[300] , *pCompleteURL = completeURL; //Create char array
	strcat(completeURL,base);
	strcat(completeURL,fragment);
	return completeURL;
}

/**********************************************************************
PURPOSE: Parse the url that was found, and return the full link 
HISTORY: Created by Brodie Taillefer, December 7,2014
INPUTS: Pointer to the full HTMLSource, with the needed link at the start
OUTPUTS: A pointer to a char that returns the usable URL extracted from the full text
ALGORITHM(S):Increase the pointer by 9 to get past the <a href=" and get right to the www link/
	relative link, we than tokenize the string splitting on the last end of the href "/>" everything
	before that will be either our full url or relative URL(just the fragment)
**********************************************************************/
char *findURL(char *URL) {
	URL+=9;
	char *link = strtok(URL,"\">"); //Tokensize the string from everything before />
	return link; //Return the link, either base or fragment
}
	
