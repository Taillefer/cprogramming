#include "examSchedule.h"	

/**********************************************************************
PURPOSE: Takes the long long unsigned int and prints its related 
HISTORY: Created by Brodie Taillefer, November 30,2014
INPUTS: The Long long unsigned int you wish to display
OUTPUTS: The binary string value of the input
ALGORITHM(S): Sets a hex mask, and begins binary AND said mask with the input
value, prints out if the value is a 1 or a 0, and right shifts, repeats until 
the value is done.
**********************************************************************/

void showBinary(lluInt myLLuVal) {
	lluInt mask = 0x8000000000000000;
	for(int ctr=0; ctr < lluBits; ctr++) {
		printf((myLLuVal & mask) ?"1":"0");
		mask >>=1;
	}
	printf("\n");
}

/**********************************************************************
PURPOSE:Take the information in the studentSchedule array, convert to a 64 bit binary number
and store in the studentSchedule array of struct.
HISTORY: Created by B.Taillefer November 30.2014
INPUTS: Student number, week, day , hour and length to enter into the array of structs
OUTPUTS: Returns 1 or 1, depending if the binary value can be entered into the structure without any conflicts
ALGORITHM(S): Takes the parameters and and turns them into a 64 bit binary value,it then checks to see if 
any of the values conflict, if they do, we cannot add the time to the array of structures. If they don't 
conflict we can binary AND them, and add it to the selected array of structures at index studentNum 
**********************************************************************/
	 

unsigned int setBookingTime(int studentNum, int week, int day, int hour, int length) {
	int time,initShift,error = 0;
	char String [64];
	char String1[64];
	char *str = String;
	char *str1 = String1;
	getBin(studentSchedules[studentNum].examWeek[week],str); //Gets the binary string of the current value in array of structs
	lluInt bookingMask = 0xFFFFFFFFFFFFFFFF; //Set the mask as Max value of 64 bits
	time = (hour - 9);  //Day starts at 9, so take the hour - 9
	initShift = lluBits - length; 
	bookingMask <<= initShift;//Left shift by initShift
	bookingMask >>= time;//Left fill with 0's up to starting time
	bookingMask >>= (day * 8);//Left fill with 8 0's for each day upto date
	getBin(bookingMask,str1); //Get the binary value of the  
	while(*str != '\0'){ //While we haven't reached the end
		if(*str == '1' && *str1 == '1') {
			error = 1;
			break;
		}
		else {
			str++;
			str1++;
		}
	}
	if(error == 0) {	//If there was no error, we can add the value to the correct array of structures
		studentSchedules[studentNum].examWeek[week] = studentSchedules[studentNum].examWeek[week] ^ bookingMask;
	}
	return error;
}

/**********************************************************************
PURPOSE: Return the times where the selected student has no exams
HISTORY: Created by B.Taillefer November 30,2014
INPUTS: The student number(array index) and the week (exam week index)
OUTPUTS: Returns the studentSchedule
ALGORITHM(S): Returns the index of the array of structures with the given studentnumber and week
NOTES: none
**********************************************************************/

lluInt getBinExamWeekInfo(int studentNum,int week) {
	return studentSchedules[studentNum].examWeek[week];
}

/**********************************************************************
PURPOSE: Get's the binary string value of the input long long unsigned int and returns adds it to the pointer
HISTORY: Created by B.Taillefer, November 30, 2014
INPUTS: Long long unsigned int to convert, and pointer to a char array
OUTPUTS: The pointer to array of char's get filled
ALGORITHM(S): Right shifts through through the mask, shifting until the value is 0
**********************************************************************/

void getBin(lluInt num, char *str) {;
	*(str+lluBits) = '\0';
	lluInt mask =  0x100000000000000 << 1;
	while(mask >>=1) {
		*str++ = !!(mask & num) + '0';
	}
}

/**********************************************************************
PURPOSE: Return the time's for the student where they have no exam's
HISTORY: Created by B.Taillefer November 30, 2014
INPUTS: Long long unsigned int to parse
OUTPUTS: The times where the student has no exams
ALGORITHM(S): Takes the 64 bit lluInt and parses it into what times are free,
Goes through every bit and see's if the value is a 1 or a 0, groups the 1's and 0's
in groups of 8, and parses the 8 bits to see what times have a 0
**********************************************************************/

void getUnscheduledTime(lluInt data) {
	lluInt x = 1; 
	int i = 0,startTime = 0,endTime = 0,count = 0,totalCount = 0,zeros = 0;
	for(i=lluBits; i>8;i--) { //Forever bit, starting at 64, ignore the last 8
		lluInt binary = data & (x << i); //Go through each bit
			if(!binary) { //If the value is a 0, increment zero, we now have free time
				count++;
				totalCount++;
				zeros++;
			}
			else { //If the value is a 1, they have an exam booked during that time
				count++;
				totalCount++;
				if(zeros > 0) { //If the value is now a 1, and there is more than one zero, that means that they they now have an exam, we must print out he free time
					startTime = ((count + 8) - (zeros)); // The Start time is equal to the number of iterations on count + 8 , minus the amount of zero's seen
					endTime = (count + 7); //The end time is rqual to the number of iterations done on count + 7;
					zeros = 0;
					printDates; //Print out the free times

				}
			}
			if((count % 8) == 0 && zeros > 0) { // If the time was all 0's for the 8 bit block then we should reset our counter and print out that the full block is free
				startTime = ((count + 9) - zeros);
				endTime = (count + 9);
				zeros = 0;
				printDates; // Print out the free times
			} 
		count%=8;	 //If count % 8 = 0 than reset count	
	}
}

	
