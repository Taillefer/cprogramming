#include <stdio.h>

#define printDates if(totalCount < 9 && endTime !=9) { \
						printf("Monday %d:00-%d:00\n",startTime, endTime);\
					} \
					else if(totalCount > 8 && totalCount <17 && endTime != 9) { \
						printf("Tuesday %d:00-%d:00\n",startTime, endTime);\
					}\
					else if(totalCount>16 && totalCount < 25 && endTime != 9) {\
						printf("Wednesday %d:00-%d:00\n",startTime, endTime);\
					}\
					else if(totalCount>24 && totalCount < 33 && endTime != 9) {\
						printf("Thursday %d:00-%d:00\n",startTime, endTime);\
					}\
					else if(totalCount>32 && totalCount < 41 && endTime != 9) {\
						printf("Friday %d:00-%d:00\n",startTime, endTime);\
					}\
					else if(totalCount>40 && totalCount < 49 && endTime != 9) {\
						printf("Saturday %d:00-%d:00\n",startTime, endTime);\
					}\
					else if(totalCount>48 && totalCount < 57 && endTime != 9) {\
						printf("Sunday %d:00-%d:00\n",startTime, endTime);\
					}
#define lluBits (8* sizeof(long long int))

#define numEntries 2864

#define numStudents 715

typedef	long long unsigned int lluInt;

struct studentSchedule{
	lluInt examWeek[2];
}studentSchedules[1000];

extern void showBinary(lluInt binary);

extern void getBin(lluInt num, char *str);

extern unsigned int setBookingTime(int studentNum, int week, int day, int hour, int length);

extern void getUnscheduledTime(lluInt data);

